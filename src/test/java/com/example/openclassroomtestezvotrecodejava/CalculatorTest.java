package com.example.openclassroomtestezvotrecodejava;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Set;

@Tag("Calculations")
@DisplayName("Test d'une classe de calculatrice")
class CalculatorTest {

    private Calculator calculator;

    private static Instant startedAt;

    @BeforeAll
    public static void executeBeforeAllTest() {
        System.out.println("Before all tests");
        startedAt = Instant.now();
    }

    @BeforeEach
    public void executeBeforeEachTest() {
        System.out.println("Before each test");
        this.calculator = new Calculator();
    }

    @AfterEach
    public void executeAfterEachTest() {
        System.out.println("After each test");
    }

    @AfterAll
    public static void executeAfterAllTest() {

        System.out.println("After all test");

        final Instant endedAt = Instant.now();

        final long duration = Duration.between(startedAt, endedAt).toMillis();

        System.out.println(MessageFormat.format("Tests duration : {0} ms", duration));
    }

    @Nested
    @Tag("CalculationSquareRootPow")
    @DisplayName("Tests sur le calcule de puissance et de racine carré")
    class SquareRootPowTests {

        private Calculator calculator;

        @BeforeEach
        public void executeBeforeEachTest() {

            this.calculator = new Calculator();
        }

        @Test
        @DisplayName("test sur le calcul de puissance")
        public void pow() {

            int a = 5;
            int pow = 2;
            int expectedResult = 25;

            assertThat(calculator.pow(a, pow)).isEqualTo(expectedResult);
        }

        @Test
        @DisplayName("test sur le calcul de racine carré")
        public void squareRoot() {
            int a = 25;
            int expectedResult = 5;

            assertThat(calculator.squareRoot(a)).isEqualTo(expectedResult);
        }

    }

    @Test
    @DisplayName("Additionne deux reels positifs")
    void testAddTwoPositiveNumbers() {
        // ARRANGE
        int a = 2;
        int b = 3;
        // ACT
        int somme = this.calculator.add(a, b);
        //ASSERT
        assertEquals(5, somme, "add test");
    }

    @Test
    @DisplayName("Multiplie deux reels positifs")
    void testAddMultiplyPositiveNumbers() {
        // ARRANGE
        int a = 2;
        int b = 3;
        // ACT
        int multiply = this.calculator.multiply(a, b);
        //ASSERT
        assertEquals(6, multiply, "multiply test");
    }

    @ParameterizedTest(name = "{0} x 0 must be equal to 0")
    @ValueSource(ints = {1, 5, 54, 426, 888})
    @DisplayName("Test la multiplication par zero sur plusieurs nombres")
    void multiplyShouldReturnZero(int arg) {
        //ARRANGE

        //ACT
        int result = calculator.multiply(arg, 0);
        //ASSERT
        assertEquals(0, result);
    }

    @ParameterizedTest(name = "{0} + {1} must be equal to {2}")
    @CsvSource({"1,1,2", "5,7,12", "17,12,29"})
    @DisplayName("Test l'addition sur plusieurs nombres et compare au résultat attendu")
    void addShouldReturnTheSum(int arg1, int arg2, int expectedSum) {
        //ARRANGE

        //ACT
        int result = calculator.add(arg1, arg2);
        //ASSERT
        assertThat(result).isEqualTo(expectedSum);
    }

    @Test
    @Timeout(3)
    @DisplayName("Test long calcule executé dans un temps imparti")
    void longCalculShouldComputeInLessThan1Second() {

        calculator.longCalculation();
    }

    @Test
    @DisplayName("Test qu'un nombre est composé de tout les chiffres")
    void testContainAllDigits() {
        //GIVEN
        int number = 95897;
        //WHEN
        Set<Integer> currentDigits = calculator.digitsSet(number);
        //THEN
        //Set<Integer> expectedDigits = Stream.of(5, 7, 8, 9).collect(Collectors.toSet());
        //assertEquals(currentDigits, expectedDigits);
        //equivalent assertJ
        assertThat(currentDigits).containsExactlyInAnyOrder(5, 7, 8, 9);
    }

    @Test
    void listDigits_shouldReturnTheListOfDigits_ofPositiveInteger() {
        //GIVEN
        int number = 95897;
        //WHEN
        Set<Integer> currentDigits = calculator.digitsSet(number);
        //THEN
        //Set<Integer> expectedDigits = Stream.of(5, 7, 8, 9).collect(Collectors.toSet());
        //assertEquals(currentDigits, expectedDigits);
        //equivalent assertJ
        assertThat(currentDigits).containsExactlyInAnyOrder(5, 7, 8, 9);
    }

    @Test
    void listDigits_shouldReturnTheListOfDigits_ofNegativeInteger() {
        //GIVEN
        int number = -423132;
        //WHEN
        Set<Integer> currentDigits = calculator.digitsSet(number);
        //THEN
        assertThat(currentDigits).containsExactlyInAnyOrder(1, 2, 3, 4);
    }

    @Test
    void listDigits_shouldReturnTheListOfDigits_ofZero() {
        //GIVEN
        int number = 0;
        //WHEN
        Set<Integer> currentDigits = calculator.digitsSet(number);
        //THEN
        assertThat(currentDigits).containsExactlyInAnyOrder(0);
    }

}
