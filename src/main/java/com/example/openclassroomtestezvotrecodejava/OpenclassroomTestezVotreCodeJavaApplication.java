package com.example.openclassroomtestezvotrecodejava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenclassroomTestezVotreCodeJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenclassroomTestezVotreCodeJavaApplication.class, args);
    }

}
