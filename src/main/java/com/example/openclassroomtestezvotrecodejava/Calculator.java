package com.example.openclassroomtestezvotrecodejava;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Calculator {

    public int add(int a, int b) {

        return a + b;
    }

    public int multiply(int a, int b) {
        return a * b;
    }

    public double add(double a, double b) {
        return a + b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public double pow(int a, int b) {
        return Math.pow(a, b);
    }

    public double squareRoot(int a) {
        return Math.sqrt(a);
    }

    public void longCalculation() {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Set<Integer> digitsSet(int number) {

        if (number == 0) return Stream.of(0).collect(Collectors.toSet());

        Set<Integer> digitSet = new HashSet<>();

        number = number < 0 ? -number : number;

        while (number != 0) {

            digitSet.add(number % 10);

            number /= 10;
        }

        return digitSet;
    }
}
